# Theme Discourse pour La Copro Des Possibles

Ce thème Discourse se base sur le thème [community de Lydra](https://lab.frogg.it/lydra/discourse/community-theme).
Il contient les éléments suivants :

- La palette de couleur de Copro Des Possibles.
- L'utilisation du [composant qui contient les polices](https://gitlab.com/coprodespossibles/composant-discourse).

## Prérequis

Vous devez installer le thème [community de Lydra](https://lab.frogg.it/lydra/discourse/community-theme) avanc celui-ci.

## Utilisation de ce thème

1. Installer le thème [community de Lydra](https://lab.frogg.it/lydra/discourse/community-theme) sur votre discourse.
2. Installer ce thème sur votre discourse.
3. Ajouter le composant `lcdp-fonts` au theme `Community Theme` dans la section `Included components`.
4. Utiliser `LCDP Light` comme `Color Palette`.
